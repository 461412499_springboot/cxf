package com.example.spring.ws.cxf.client.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.apache.cxf.binding.soap.SoapHeader;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.headers.Header;
import org.apache.cxf.helpers.DOMUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.namespace.QName;
import java.util.List;
import java.util.UUID;

@Slf4j
@Component
public class AuthIntercepter extends AbstractPhaseInterceptor<SoapMessage> {
    public AuthIntercepter() {
        super(Phase.PREPARE_SEND);
    }

    @Override
    public void handleMessage(SoapMessage message) throws Fault {
        log.info("增加权限。。");
        List<Header> headers = message.getHeaders();
        Document document = DOMUtils.createDocument();
        Element auth = document.createElement("auth");
        Element token = document.createElement("token");
        token.setTextContent(UUID.randomUUID().toString());
        auth.appendChild(token);
        SoapHeader soapHeader = new SoapHeader(new QName(""), auth);
        headers.add(soapHeader);
    }
}
