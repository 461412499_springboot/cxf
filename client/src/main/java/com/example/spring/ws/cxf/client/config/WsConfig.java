package com.example.spring.ws.cxf.client.config;

import com.example.spring.ws.cxf.client.consts.WsConst;
import com.example.spring.ws.cxf.client.interceptor.AuthIntercepter;
import com.example.spring.ws.cxf.server.webservice.AuthorServicePort;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.JaxWsClientFactoryBean;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WsConfig {

    @Autowired
    private AuthIntercepter authIntercepter;

    @Bean("cxfProxy")
    public AuthorServicePort createAuthorServicePort(){
        JaxWsProxyFactoryBean jaxWsProxyFactoryBean=new JaxWsProxyFactoryBean();
        jaxWsProxyFactoryBean.setServiceClass(AuthorServicePort.class);
        jaxWsProxyFactoryBean.setAddress(WsConst.SERVICE_ADDRESS);
        jaxWsProxyFactoryBean.getOutInterceptors().add(authIntercepter);
        return (AuthorServicePort) jaxWsProxyFactoryBean.create();
    }

//    @Bean
//    public Client createDynamicClient(){
//        JaxWsDynamicClientFactory jaxWsDynamicClientFactory=JaxWsDynamicClientFactory.newInstance();
//        Client client = jaxWsDynamicClientFactory.createClient(WsConst.SERVICE_ADDRESS);
//        return client;
//    }
}
