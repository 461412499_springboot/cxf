package com.example.spring.ws.cxf.client.consts;

public class WsConst {

    public static final String NAMESPASE_URI="http://www.server.cxf.ws.spring.EXAMPLE.com/webservice";

    public static final String SERVICE_ADDRESS= "http://127.0.0.1:8080/ws/author?wsdl";

}
