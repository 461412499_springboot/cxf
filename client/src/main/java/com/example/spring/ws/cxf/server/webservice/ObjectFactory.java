
package com.example.spring.ws.cxf.server.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.example.spring.ws.cxf.server.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetAuthorInfo_QNAME = new QName("http://www.server.cxf.ws.spring.EXAMPLE.com/webservice", "getAuthorInfo");
    private final static QName _GetAuthorInfoResponse_QNAME = new QName("http://www.server.cxf.ws.spring.EXAMPLE.com/webservice", "getAuthorInfoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.example.spring.ws.cxf.server.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetAuthorInfo }
     * 
     */
    public GetAuthorInfo createGetAuthorInfo() {
        return new GetAuthorInfo();
    }

    /**
     * Create an instance of {@link GetAuthorInfoResponse }
     * 
     */
    public GetAuthorInfoResponse createGetAuthorInfoResponse() {
        return new GetAuthorInfoResponse();
    }

    /**
     * Create an instance of {@link Author }
     * 
     */
    public Author createAuthor() {
        return new Author();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAuthorInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.server.cxf.ws.spring.EXAMPLE.com/webservice", name = "getAuthorInfo")
    public JAXBElement<GetAuthorInfo> createGetAuthorInfo(GetAuthorInfo value) {
        return new JAXBElement<GetAuthorInfo>(_GetAuthorInfo_QNAME, GetAuthorInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAuthorInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.server.cxf.ws.spring.EXAMPLE.com/webservice", name = "getAuthorInfoResponse")
    public JAXBElement<GetAuthorInfoResponse> createGetAuthorInfoResponse(GetAuthorInfoResponse value) {
        return new JAXBElement<GetAuthorInfoResponse>(_GetAuthorInfoResponse_QNAME, GetAuthorInfoResponse.class, null, value);
    }

}
