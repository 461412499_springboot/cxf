package com.example.spring.ws.cxf.client.controller;

import com.example.spring.ws.cxf.server.webservice.Author;
import com.example.spring.ws.cxf.server.webservice.AuthorServicePort;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class indexController {

//    @Autowired
//    private Client client;

    @Resource(name = "cxfProxy")
    private AuthorServicePort authorServicePort;
    @RequestMapping("/index")
    public Author getAuthor(int id){
        return authorServicePort.getAuthorInfo(id);
    }

    @RequestMapping("/indexnoparam")
    public Author getAuthor(){
        return authorServicePort.getAuthorInfo(1);
    }

}
