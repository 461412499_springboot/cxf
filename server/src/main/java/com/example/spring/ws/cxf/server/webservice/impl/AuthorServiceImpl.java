package com.example.spring.ws.cxf.server.webservice.impl;

import com.example.spring.ws.cxf.server.consts.WsConst;
import com.example.spring.ws.cxf.server.pojo.Author;
import com.example.spring.ws.cxf.server.webservice.AuthorService;
import org.springframework.stereotype.Service;

import javax.jws.WebService;

@WebService(targetNamespace = WsConst.NAMESPASE_URI, name = "AuthorService", serviceName = "AuthorService", portName = "AuthorPortname", endpointInterface = "com.example.spring.ws.cxf.server.webservice.AuthorService")
@Service
public class AuthorServiceImpl implements AuthorService {

    @Override
    public Author getAuthorInfo(int id) {
        Author author=new Author();
        author.setId(id);
        author.setName("yue");
        author.setAge(19);
        return author;
    }
}
