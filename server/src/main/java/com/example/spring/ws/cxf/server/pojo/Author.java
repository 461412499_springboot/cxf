package com.example.spring.ws.cxf.server.pojo;

import lombok.Data;

@Data
public class Author {

    private int id;

    private String name;

    private int age;
}
