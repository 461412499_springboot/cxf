package com.example.spring.ws.cxf.server.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.headers.Header;
import org.apache.cxf.helpers.DOMUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.namespace.QName;
import java.awt.*;
import java.util.List;
import java.util.UUID;

@Slf4j
@Component
public class InInterceptor extends AbstractPhaseInterceptor<SoapMessage> {

    public InInterceptor() {
        super(Phase.PRE_INVOKE);
    }

    @Override
    public void handleMessage(SoapMessage message) throws Fault {
        log.info("拦截器开始校验");
        List<Header> headers = message.getHeaders();
        if(headers==null || headers.size()<1){
            throw new IllegalArgumentException("check fail，please input valid argument（40001）");
        }
        Header header = headers.get(0);

        Element element= (Element) header.getObject();
        NodeList auth = element.getElementsByTagName("token");
        if(auth==null||auth.getLength()<1){
            throw new IllegalArgumentException("check fail，please input valid argument（40002）");
        }
        String token = auth.item(0).getTextContent();
        log.info("token", token);


    }
}
