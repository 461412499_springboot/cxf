package com.example.spring.ws.cxf.server.webservice;

import com.example.spring.ws.cxf.server.consts.WsConst;
import com.example.spring.ws.cxf.server.pojo.Author;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(targetNamespace = WsConst.NAMESPASE_URI,name = "AuthorServicePort")
public interface AuthorService {

    @WebMethod
    Author getAuthorInfo(@WebParam(name="id")int id);
}
