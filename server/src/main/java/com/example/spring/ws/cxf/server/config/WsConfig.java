package com.example.spring.ws.cxf.server.config;

import com.example.spring.ws.cxf.server.interceptor.InInterceptor;
import com.example.spring.ws.cxf.server.webservice.AuthorService;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.CXFService;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

@Configuration
public class WsConfig {

    @Autowired
    private SpringBus springBus;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private InInterceptor inInterceptor;

    @Bean("cxfServletRegistration")
    public ServletRegistrationBean dispatcherServlet(){
        return new ServletRegistrationBean(new CXFServlet(),"/ws/*");
    }

    @Bean
    public Endpoint publisherEndpoint(){
        EndpointImpl endpoint=new EndpointImpl(springBus,authorService);
        endpoint.publish("/author");
        endpoint.getInInterceptors().add(inInterceptor);
        return endpoint;
    }
}
